# Fluentd Daemonset for Kubernetes

Repository to build our custom image with fluentd HTTP plugin to integrate
Magnum logs with the CERN Monitoring infrastructure.

Based on: https://github.com/fluent/fluentd-kubernetes-daemonset
